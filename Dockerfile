FROM node:lts
RUN mkdir -p /var/www/html/app/weather-app-docker
COPY ./ /var/www/html/app/weather-app-docker/
RUN chown -R root:root /var/www/html/app/
WORKDIR /var/www/html/app/weather-app-docker/
RUN npm install
ENTRYPOINT npm run dev
