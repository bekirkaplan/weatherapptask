# weather-app

This template should help get you started developing with Vue 3 in Vite.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin). (You should also restart you IDE, volar plugin will give you 'component is declared but its value is never read.' error messages)

## Type Support for `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin) to make the TypeScript language service aware of `.vue` types.

If the standalone TypeScript plugin doesn't feel fast enough to you, Volar has also implemented a [Take Over Mode](https://github.com/johnsoncodehk/volar/discussions/471#discussioncomment-1361669) that is more performant. You can enable it by the following steps:

1. Disable the built-in TypeScript Extension
   1. Run `Extensions: Show Built-in Extensions` from VSCode's command palette
   2. Find `TypeScript and JavaScript Language Features`, right click and select `Disable (Workspace)`
2. Reload the VSCode window by running `Developer: Reload Window` from the command palette.

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

### Run End-to-End Tests with [Cypress](https://www.cypress.io/)

#- !!! Remove "--host" command inside the package.json->scripts->dev script.

```sh
npm run build
npm run test:e2e # or `npm run test:e2e:ci` for headless testing
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

### AWS [WeatherApp](http://13.37.107.143:8000/)

```sh

# local build docker image
vim build.sh # enter "docker build -t my ."
chmod +x build.sh # make sh excecutable
# connection configuration
vim config # aws Ec2 instance run and change hostname with aws instance hostname
# when aws instance restarted, instance ip changed then we must reenter to config file in ssh
cd ~/.ssh/ # local computer aws ssh folder
ssh aws-weatherapp # connect aws then click yes
./stop.sh # then yes
./start.sh # get new ip from aws and run on :8000 port
#send local folder to aws Ec2 instance
scp -r weather-app-docker/ aws-weatherapp:/home/ubuntu/weather-app-docker/ # send local to aws ec2
```

### Npm Dependencies

```sh
"dependencies": {
   "axios": "^0.27.2", # http client
   "dayjs": "^1.11.3", # date time library light weight and fast
   "pinia": "^2.0.14", # vue recommended simple but powerfull state manager
   "vue": "^3.2.36", # vue framework
   "vue-router": "^4.0.15", # spa page routing library
   "xss": "^1.0.13" # for simple xss attacs checking
},
```
