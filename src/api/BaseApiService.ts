import { FetchError, getErrorResponseMessage } from '@/helpers/ErrorHandling'
import { handleResponseError } from '@/helpers/Sentry'
import axios, { type AxiosInstance } from 'axios'
import { useWeatherStore } from '@/stores/WeatherStore'
import type { IBaseApiConstructorData } from '@/types/WeatherAppTypes'
import type { ENUMS } from '@/types/Enums'
abstract class BaseApiService {
  private baseApiData: IBaseApiConstructorData = {} as IBaseApiConstructorData
  public axiosInstance: AxiosInstance = {} as AxiosInstance

  constructor(baseApiData: IBaseApiConstructorData) {
    if (!baseApiData) return

    this.baseApiData = baseApiData
    this.axiosInstance = axios.create({
      baseURL: this.baseApiData.baseUrl,
      timeout: this.baseApiData.requestTimeout
        ? this.baseApiData.requestTimeout
        : 3000,
      headers: this.getHeaders()
    })

    this.axiosInstance.interceptors.response.use(
      (res) => res.data,
      (error) => {
        // sent response errors to sentry
        handleResponseError(error)

        throw new FetchError({
          status: error.status,
          error: error.error,
          message: getErrorResponseMessage(error),
          path: error.path,
          url: error.url,
          body: error.body
        })
      }
    )
  }

  public getWeatherStore(): any {
    return useWeatherStore()
  }

  public getApiKey(): ENUMS {
    return this.baseApiData.apiKey
  }

  private getHeaders() {
    const headers = { 'Content-Type': 'application/json' }
    return headers
  }
}

export default BaseApiService
