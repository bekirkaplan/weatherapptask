import type {
  Coord,
  IFindDataResponse,
  IOneCall
} from '@/types/OpenWeatherTypes'
import BaseApiService from './BaseApiService'
import { ENUMS } from '../types/Enums'
import { getCleanString } from '@/helpers/Security'
import type {
  IBaseApi,
  IBaseApiConstructorData,
  IWeatherAppFindData
} from '@/types/WeatherAppTypes'
import { mapperOpenWeather } from '@/helpers/ApiMapper'

class OpenWeatherMapService extends BaseApiService implements IBaseApi {
  constructor(baseApiData: IBaseApiConstructorData) {
    super(baseApiData)
  }

  public async getCities(name: string): Promise<void> {
    const cleanName = getCleanString(name)
    const cityList: IFindDataResponse = await this.axiosInstance.get(
      `/find?q=${cleanName}&appid=${this.getApiKey()}`
    )
    const convertedData: IWeatherAppFindData[] =
      mapperOpenWeather.mapCities(cityList)

    this.getWeatherStore().cityList().set(convertedData)
  }

  public async getWeatherForecast(coord: Coord): Promise<void> {
    const weatherStoreData: IOneCall = await this.axiosInstance.get(
      `/onecall?lat=${coord.lat}&lon=${
        coord.lon
      }&units=metric&appid=${this.getApiKey()}`
    )
    const convertedData = mapperOpenWeather.mapWeatherForecast(
      weatherStoreData,
      this.getWeatherStore().getCityName
    )

    this.getWeatherStore().weatherForecast().set(convertedData)
  }
}

export default new OpenWeatherMapService({
  baseUrl: 'https://api.openweathermap.org/data/2.5/',
  requestTimeout: 5000,
  apiKey: ENUMS.OPEN_WEATHER_API_KEY
})
