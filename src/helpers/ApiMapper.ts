/**
 * Map the API data from api response objects to IWeatherAppData
 *
 */
import type {
  IDaily,
  IFindData,
  IFindDataResponse,
  IOneCall
} from '@/types/OpenWeatherTypes'

import type {
  IWeatherAppData,
  IWeatherAppFindData
} from '@/types/WeatherAppTypes'

/**
 * Mapper function for OpenWeatherMap API
 * this implementation is used for converting data from OpenWeatherMap API to
 * WeatherApp data type.
 *
 * Requests are in the OpenWeatherMapService.ts
 */
export const mapperOpenWeather = {
  // mapper for find request response
  mapCities: (apiFindData: IFindDataResponse): IWeatherAppFindData[] => {
    const findData = apiFindData.list.map((data: IFindData) => {
      return {
        icon: new URL(
          `http://openweathermap.org/img/wn/${data.weather?.[0].icon}@4x.png`
        ),
        cityName: data.name,
        dateTime: data.main.temp_max,
        tempMax: Math.round(data.main.temp),
        coordinates: data.coord,
        sys: data.sys
      }
    })
    return findData
  },
  // mapper for onecall request data response
  mapWeatherForecast: (data: IOneCall, cityName: string): IWeatherAppData[] => {
    const daily = data.daily.map((day: IDaily) => {
      return {
        cityName: cityName,
        icon: new URL(
          `http://openweathermap.org/img/wn/${day.weather?.[0].icon}@4x.png`
        ),
        dateTime: day.dt,
        windSpeed: day.wind_speed,
        humidity: day.humidity,
        temperature: Math.round(day.temp?.max),
        timezone: data.timezone,
        timezoneOffset: data.timezone_offset
      } as IWeatherAppData
    })
    return daily
  }
}
