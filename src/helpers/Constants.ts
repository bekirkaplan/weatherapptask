import type { Environment } from '@/types/CommonTypes'
import { URLS } from '@/types/Enums'

const hostName = window.location.hostname
export const ENVIRONMENT: Environment = (() => {
  if (hostName.includes(URLS.PRODUCTION)) {
    return 'production'
  }

  if (hostName.includes('weatherapp')) {
    return 'development'
  }

  return 'localhost'
})()
