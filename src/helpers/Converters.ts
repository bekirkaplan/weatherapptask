export const doubleToInt = (num: string): string => {
  return parseInt(num, 10) ? parseInt(num, 10).toString() : ''
}
export const capitalizeFirstLetter = (text: string) => {
  return text.charAt(0).toUpperCase() + text.slice(1)
}
