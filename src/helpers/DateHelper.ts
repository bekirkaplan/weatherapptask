import dayjs from 'dayjs'
import timezone from 'dayjs/plugin/timezone'
import utc from 'dayjs/plugin/utc'

dayjs.extend(utc)
dayjs.extend(timezone)

export const unixTimeToDateTime = (
  unixTime: number,
  timezonex: string,
  type?: 'time' | 'date' | ' date-time'
): string => {
  if (!unixTime) return ''

  const format =
    type === 'date'
      ? 'DD/MM/YYYY'
      : type === 'time'
      ? 'hh:mm'
      : 'DD/MM/YYYY HH:mm'

  const timess = dayjs.unix(unixTime)
  const timezz = dayjs.tz(timess, timezonex).format(format)
  return timezz
}
