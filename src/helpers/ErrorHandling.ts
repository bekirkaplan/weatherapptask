import type { IErrorType, INewRestError } from '@/types/ErrorTypes'

// parse and sent error message from error message, description, error
export function getErrorResponseMessage(response: IErrorType) {
  const message = response
    ? response.message || response.description || response.error
    : response

  if (message) {
    if (message.includes('(code')) {
      return message.split('(code')[0]
    } else {
      return message
    }
  }

  return 'Something went wrong, please try again later'
}

// fetch error class extends Error
export class FetchError extends Error {
  public status: number // for example 400
  public error?: string // for example "Bad Request"
  public path?: string
  public url?: string // for example "Bad Request"
  public body?: string // for example {}

  constructor({ status, error, message, path, url, body }: INewRestError) {
    super(message)
    // any subclass of FooError will have to manually set the prototype
    Object.setPrototypeOf(this, new.target.prototype) // restore prototype chain

    this.error = error
    this.status = status
    this.message = message
    this.path = path
    this.url = url
    this.body = body
  }

  public toString() {
    return `FetchError: ${this.error || this.status}. ${this.message}`
  }
}
