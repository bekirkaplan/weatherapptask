import XSS from 'xss'

// check string before sending request to the api
export const getCleanString = (data: string): string => {
  return XSS(
    data
      .replace(/<.*?>/g, '')
      .normalize('NFD')
      .replace(/[\u0300-\u036f]/g, '')
  )
}
