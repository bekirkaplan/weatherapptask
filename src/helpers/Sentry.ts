import * as Sentry from '@sentry/vue'
import { BrowserTracing } from '@sentry/tracing'
import pjson from '../../package.json'
import { ENVIRONMENT } from '@/helpers/Constants'
import type { Router } from 'vue-router'
import type { App } from 'vue'
import { getErrorResponseMessage } from './ErrorHandling'
import { E_SENTRY } from '@/types/Enums'
import type { IErrorType } from '@/types/ErrorTypes'

// Start sentry when not developing
export function initializeSentry(router: Router, app: App<Element>) {
  if (ENVIRONMENT !== 'localhost') {
    Sentry.init({
      app,
      dsn: `https://${E_SENTRY.API_KEY}.sentry.io/6526455`,
      integrations: [
        new BrowserTracing({
          routingInstrumentation: Sentry.vueRouterInstrumentation(router),
          tracingOrigins: ['localhost', `${E_SENTRY.API_KEY}/`, /^\//]
        })
      ],
      release: pjson.version,
      // Set tracesSampleRate to 1.0 to capture 100%
      // of transactions for performance monitoring.
      // We recommend adjusting this value in production
      tracesSampleRate: 1.0
    })
  }
}

export function sendExceptionToSentry(error?: any, errorInfo?: any) {
  if (!error) {
    return
  }

  // Prevent default messages to be send to sentry
  let doNotSend = false
  ERROR_EXCEPTIONS.forEach((exceptionError) => {
    if (error.message.includes(exceptionError)) {
      doNotSend = true
    }
  })

  if (doNotSend) {
    return
  }

  // Send to sentry
  Sentry.withScope((scope) => {
    if (errorInfo) {
      Object.keys(errorInfo).forEach((key) => {
        scope.setExtra(key, errorInfo[key])
      })
    }

    Sentry.captureException(error)
  })
}

// sending extra information to the sentry.
export function sendMessageToSentry(message: string, messageInfo?: any) {
  Sentry.withScope((scope) => {
    if (messageInfo) {
      Object.keys(messageInfo).forEach((key) => {
        scope.setExtra(key, messageInfo[key])
      })
    }

    Sentry.captureMessage(message)
  })
}

// Base api service axios interceptor calls this  method sent the response messages from ApiSservices
// and sent response messages to sentry service
export function handleResponseError(err: IErrorType) {
  if (err && err.status === 401) {
    return 'Unauthorized'
  }

  const errMessage = getErrorResponseMessage(err)

  if (err && err.status !== 404) {
    sendExceptionToSentry(err, {
      message: errMessage,
      error: err.error,
      status: err.status,
      path: err.path,
      url: err.url,
      body: err.body,
      header: err.headers
    })
  }
}

const ERROR_EXCEPTIONS: string[] = [
  'A server with the specified hostname could not be found.',
  'Failed to fetch'
]
