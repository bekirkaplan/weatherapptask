import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'
import { initializeSentry } from '@/helpers/Sentry'

const pinia = createPinia()

// Create an app instance
const app = createApp(App)

initializeSentry(router, app)

app.use(pinia) // inject pinia to app instance
app.use(router) // inject router to app instance

// when router is ready then mount the app to html element with id app
router.isReady().then(() => {
  app.mount('#app')
})
