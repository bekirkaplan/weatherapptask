/**
 * Pinie store implementation
 */
import { defineStore } from 'pinia'
import owService from '../api/OpenWeatherMapService'
import type {
  IStoreObject,
  IWeatherAppData,
  IWeatherAppFindData
} from '@/types/WeatherAppTypes'
import type { Coord } from '@/types/OpenWeatherTypes'
import { capitalizeFirstLetter } from '@/helpers/Converters'

const getSelectedStore = (selection = 'ows') => {
  if (selection && selection === 'ows') {
    return owService
  }
  return owService
}

export const useWeatherStore = defineStore({
  id: 'weatherStore',
  state: () =>
    ({
      cityName: '' as string,
      cities: [] as IWeatherAppFindData[],
      weatherCurrent: {} as IWeatherAppData,
      weatherForecastList: [] as IWeatherAppData[],
      listSortState: false
    } as IStoreObject),
  actions: {
    cityList() {
      return {
        get: (name: string) => {
          getSelectedStore('ows').getCities(name)
          this.cityName = capitalizeFirstLetter(name)
        },
        set: (cityList: IWeatherAppFindData[]) => {
          this.cities = cityList
        }
      }
    },
    weatherForecast() {
      return {
        get: (coord: Coord) => {
          getSelectedStore().getWeatherForecast(coord)
        },
        set: (convertedData: IWeatherAppData[]) => {
          this.weatherForecastList = convertedData.slice(1, 6)
          this.weatherCurrent = this.weatherForecastList[0]
        }
      }
    },
    resetAll() {
      this.cities = [] as IWeatherAppFindData[]
      this.weatherForecastList = [] as IWeatherAppData[]
      this.listSortState = false
    },
    reverseList() {
      this.listSortState = !this.listSortState
      this.weatherForecastList = this.weatherForecastList.reverse()
    }
  },
  getters: {
    getCityName: (state) => state.cityName,
    getCities: (state) => state.cities,
    getWeatherCurrent: (state) => state.weatherCurrent,
    getWeatherForecastList: (state) => state.weatherForecastList,
    getListSortState: (state) => state.listSortState
  }
})
