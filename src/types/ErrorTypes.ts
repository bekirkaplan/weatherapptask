export interface INewRestError {
  status: number
  error: string
  message: string
  path?: string
  url?: string
  body?: any
}

export interface IErrorType {
  message: any
  description: any
  error: any
  status: number
  path?: string
  url?: string
  body?: string
  headers?: any
}
