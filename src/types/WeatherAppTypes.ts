/**
 * Weather App Types is currently used by our weather app project.
 * Please use "I" character if you decided to use outside the types
 */
import type { URL } from 'url'
import type { ENUMS } from './Enums'

export type MetricImperial = 'metric' | 'imperial'
export interface Coord {
  lon: number
  lat: number
}
export interface IWeatherAppData {
  icon: URL
  cityName: string
  dateTime: number
  timezone?: string
  timezoneOffset?: number
  windSpeed: number
  humidity: number
  temperature: number
  coordinates?: Coord
  units?: MetricImperial
}

export interface IWeatherAppFindData {
  icon: URL
  cityName: string
  coordinates: Coord
  tempMax: number
  dateTime: number
  sys: IFindSys
}
export interface IFindSys {
  country: string
}

export interface IStoreObject {
  cityName: string
  cities: IWeatherAppFindData[] // cities returns from Api
  weatherCurrent: IWeatherAppData // to hold onw col dow
  weatherForecastList: IWeatherAppData[] //
  listSortState: boolean
}

export interface IBaseApiConstructorData {
  baseUrl: string
  requestTimeout?: number
  apiKey: ENUMS
}

export interface IBaseApi {
  getCities: (val: string) => any
  getWeatherForecast: (val: Coord) => any
}
