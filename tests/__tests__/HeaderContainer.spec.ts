import { describe, expect, it } from 'vitest'
import { mount } from '@vue/test-utils'
import MainHeader from '@/components/Common/MainHeader.vue'

/**
 * Project main header test with checking header welcome text
 * and HeaderContainer must have a string msg prop to get the header text
 * check if msg and prontet string are equal
 */
describe('MainHeader', () => {
  it('greetings contains h1', () => {
    const wrapper = mount(MainHeader)
    expect(wrapper.find('.greetings').find('h1')).exist
    expect(wrapper.find('.greetings').findAll('h1').length).toBeLessThan(2)
  })
  it('renders properly', () => {
    const wrapper = mount(MainHeader, {
      props: { msg: 'Weather Forecast' }
    })
    expect(wrapper.find('h1').text()).toContain('Weather Forecast')
  })
})
