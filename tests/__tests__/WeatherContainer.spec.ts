import { createPinia, setActivePinia } from 'pinia'
import { beforeAll, describe, expect, it } from 'vitest'
import { mount } from '@vue/test-utils'
import WeatherContainer from '@/components/Weathers/WeatherContainer.vue'
import WeatherCard from '@/components/Weathers/WeatherCard.vue'

beforeAll(() => {
  setActivePinia(createPinia())
})
/**
 * Check if weather card item existance in first load. Expected false
 */
describe('WeatherContainer', () => {
  it('Check WeatherItem not exist', () => {
    const wrapper = mount(WeatherContainer)
    expect(wrapper.findComponent(WeatherCard).exists()).toBe(false)
  })
})
