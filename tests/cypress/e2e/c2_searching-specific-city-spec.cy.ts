import landingPage from '../page-objects/landing-page'

describe('Searching specific city', () => {
  it('Opening base url', () => {
    cy.visit('/')
    landingPage.checkInitialElements()
    landingPage.setSearchInput('Ankara')
    landingPage.clickSearchButton()
    landingPage.checkSearchedWidgets()
  })
})
