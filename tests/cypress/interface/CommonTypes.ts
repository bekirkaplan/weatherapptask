/**
 * Common types, general use purpose
 * If you have common interfaces for general use purpose we can define them here.
 */
export interface IKeyValueStore {
  [key: string]: string
}
