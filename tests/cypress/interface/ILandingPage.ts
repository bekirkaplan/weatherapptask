import type { IKeyValueStore } from './CommonTypes'

export interface InitialElms extends IKeyValueStore {
  searchInput: string
  searchButton: string
}

export interface SearchElms extends IKeyValueStore {
  detailedFirstWidget: string
  detailedSecondWidget: string
  detailedThirdWidget: string
  detailedFourthWidget: string
  detailedFifthWidget: string
}

export interface ILandingPage {
  initialElms: InitialElms
  searchElms: SearchElms
  collection: IKeyValueStore
}
