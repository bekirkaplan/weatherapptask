import type {
  ILandingPage,
  InitialElms,
  SearchElms
} from '../interface/ILandingPage'

class LandingPage {
  fields: ILandingPage

  constructor() {
    const initialElms: InitialElms = {
      welcomeText: '[data-cy="header-container"] h1',
      searchInput: '[data-cy="location-search-cy"] input',
      searchButton: '[data-cy="location-search-cy"] button'
    }

    const collection = {
      cityButton: '[data-cy="location-city-cy0"]'
    }

    const searchElms: SearchElms = {
      detailedFirstWidget:
        '[data-cy="weather-card-list-container-cy"] div:nth-child(1)',
      detailedSecondWidget:
        '[data-cy="weather-card-list-container-cy"] div:nth-child(2)',
      detailedThirdWidget:
        '[data-cy="weather-card-list-container-cy"] div:nth-child(3)',
      detailedFourthWidget:
        '[data-cy="weather-card-list-container-cy"] div:nth-child(4)',
      detailedFifthWidget:
        '[data-cy="weather-card-list-container-cy"] div:nth-child(5)'
    }

    this.fields = {
      initialElms,
      searchElms,
      collection
    }
  }

  // step 1: check inital elemens if visible
  checkInitialElements() {
    for (const argumentsKey in this.fields.initialElms) {
      cy.get(this.fields.initialElms[argumentsKey], { timeout: 2000 }).should(
        'be.visible'
      )
    }
  }

  // step 2: fill the search input field with a given city value
  setSearchInput(searchWord: string) {
    cy.get(this.fields.initialElms.searchButton).should('be.disabled')
    cy.get(this.fields.initialElms.searchInput).type(searchWord)
    cy.get(this.fields.initialElms.searchButton).should('be.enabled')
  }

  // step 3: click the search button to send request to api
  clickSearchButton() {
    cy.get(this.fields.initialElms.searchButton).click()
    cy.get(this.fields.collection.cityButton, { timeout: 5000 }).should(
      'be.visible'
    )
    cy.get(this.fields.collection.cityButton).click()
  }

  // TODO: we can check store state here

  // step 4: check if the widgets created by vue with using api result
  checkSearchedWidgets() {
    cy.get(this.fields.collection.cityButton, { timeout: 2000 }).should(
      'not.exist'
    )
    for (const argumentsKey in this.fields.searchElms) {
      cy.get(this.fields.searchElms[argumentsKey], { timeout: 2000 }).should(
        'be.visible'
      )
    }
  }
}

export default new LandingPage()
